#include <iostream>

//Call function std::cout
void print()
{
    //Use satndart lib
    std::cout << "Hello Skillbox!\n";
}
/*
*Assign value to variables; print "Hello World!"
*/
int main()
{
    int x = 100;
    int y = x + 100;

    int b = 0;
    b = b + 2;

    int test01;
    test01 = 100500;

    int test02 = 100501;

    int mult = x * y;
    int random = 1002;

    //Print our variables
    std::cout << "Hello World!\n\nOur Variables:\nRandom = ";
    std::cout << random;

    std::cout << "\nx = ";
    std::cout << x;

    std::cout << "\ny = ";
    std::cout << y;

    std::cout << "\nb = ";
    std::cout << b;

    std::cout << "\ntest01 = ";
    std::cout << test01;

    std::cout << "\ntest02 = ";
    std::cout << test02;

    std::cout << "\n";
}